package jp.co.fis.domain;

import java.util.Collections;
import java.util.List;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
@ToString
@EqualsAndHashCode
public class DateDurations {

	List<DateDuration> dateDurations;

	/**
	 * コンストラクタ
	 * 
	 * @param dateDurations
	 *            設定する期間のリスト <code>null</code>や空リストの場合は空の状態を保持
	 */
	public DateDurations(List<DateDuration> dateDurations) {
		if (dateDurations == null || dateDurations.isEmpty()) {
			this.dateDurations = Collections.emptyList();
		} else {
			this.dateDurations = dateDurations;
		}
	}

}
