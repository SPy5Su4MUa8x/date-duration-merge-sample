package jp.co.fis.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DateDurationMerge {

	private final DateDurationComparator comparator;

	public DateDurationMerge() {
		this.comparator = new DateDurationComparator();
	}

	public DateDurations execute(DateDurations baseDurations, DateDurations newDurations) throws Exception {
		log.debug("{}, {}", baseDurations, newDurations);
		Collections.sort(baseDurations.getDateDurations(), comparator);
		Collections.sort(newDurations.getDateDurations(), comparator);

		List<DateDuration> newBaseAll = new ArrayList<>();

		for (DateDuration base : baseDurations.getDateDurations()) {
			List<DateDuration> newBase = new ArrayList<>();
			newBase.add(base);

			for (DateDuration _new : newDurations.getDateDurations()) {
				newBase = cutDuration(newBase, _new);
			}

			newBaseAll.addAll(newBase);
			newBase.clear();
		}

		// 新Baseとnewを1つに纏めてソート
		List<DateDuration> merged = new ArrayList<>();
		merged.addAll(newBaseAll);
		merged.addAll(newDurations.getDateDurations());
		Collections.sort(merged, comparator);
		log.debug("{}", merged);
		return new DateDurations(merged);
	}

	/**
	 * 2つの期間について、1日でも重なっている日があるか判定する。
	 * 
	 * @param base
	 *            基準とする期間を保持するインスタンス
	 * @param target
	 *            比較対象期間を保持するインスタンス
	 * @return 判定結果<li><code>true</code>：重なっている日がある<li><code>false</code>
	 *         ：重なっている日はない
	 */
	private boolean overlap(DateDuration base, DateDuration target) {
		boolean isOverlap = (base.getFrom().compareTo(target.getTo()) <= 0)
				&& (target.getFrom().compareTo(base.getTo()) <= 0);
		return isOverlap;
	}

	private List<DateDuration> cutDuration(List<DateDuration> newBase, DateDuration _new) throws Exception {

		List<DateDuration> result = new ArrayList<>();

		DateDuration n = new DateDuration(_new.getFrom(), _new.getTo());
		for (DateDuration b : newBase) {

			// 元の期間がマージ対象と全く重なってない時、元の期間は切らずにそのまま残す。
			if (!overlap(b, n)) {
				result.add(b);
				continue;
			}

			// 1.後ろを残すパターン
			// new.開始日≦base.開始日 AND new.終了日＜base.終了日
			// 【イメージ】※受領の開始は、格納の開始以前であればいつでもいい。
			// b <------------------------------------------->
			// n <========================================>
			// r <========================================><->
			if ((n.getFrom().isBefore(b.getFrom()) || n.getFrom().compareTo(
					b.getFrom()) == 0)
					&& n.getTo().isBefore(b.getTo())) {
				// newのToの翌日を、baseのFromにセット
				DateDuration cpBase = new DateDuration(n.getTo().plusDays(1), b.getTo());
				result.add(cpBase);
				continue;
			}

			// 2.後ろと前を残すパターン
			// 【イメージ】※newの期間は、baseの開始終了と同日を含まない物であればいつでもいい。
			// b <--------------------------------------------->
			// n　　　　<==============================>
			// r <-----><==============================><------>
			if (n.getFrom().isAfter(b.getFrom()) && n.getTo().isBefore(b.getTo())) {
				DateDuration front = new DateDuration(b.getFrom(), n.getFrom().plusDays(-1));
				DateDuration back = new DateDuration(n.getTo().plusDays(1), b.getTo());
				result.add(front);
				result.add(back);
				continue;
			}

			// 3.前を残すパターン
			// 【イメージ】※newの終了はbaseの終了以降であればいつでもいい。
			// b <------------------------------------------->
			// n　　<========================================>
			// r <-><========================================>
			if (b.getFrom().isBefore(n.getFrom())
					&& (b.getTo().compareTo(n.getTo()) == 0 || b.getTo().isBefore(
							n.getTo()))) {
				DateDuration cpBase = new DateDuration(b.getFrom(), n.getFrom().plusDays(-1));
				result.add(cpBase);
				continue;
			}
		}

		return result;

	}

}
