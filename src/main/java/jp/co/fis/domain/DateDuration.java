package jp.co.fis.domain;

import java.time.LocalDate;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
@ToString
@EqualsAndHashCode
public class DateDuration {

	LocalDate from;
	LocalDate to;

	/**
	 * コンストラクタ
	 * 
	 * @param from
	 *            設定する期間の開始日付
	 * @param to
	 *            設定する期間の終了日付
	 */
	public DateDuration(LocalDate from, LocalDate to) {
		super();

		if (from == null || to == null) {
			throw new IllegalArgumentException("from、toのいずれかがnullです。");
		}

		if (from.isAfter(to)) {
			throw new IllegalArgumentException("fromとtoの前後関係が逆です。from≦toとなるようにしてください。");
		}

		this.from = from;
		this.to = to;
	}

}