package jp.co.fis.domain;

import java.util.Comparator;

public class DateDurationComparator implements Comparator<DateDuration> {

	@Override
	public int compare(DateDuration o1, DateDuration o2) {
		int compareStart = o1.getFrom().compareTo(o2.getFrom());
		if (compareStart == 0) {
			int compareEnd = o1.getTo().compareTo(o2.getTo());
			if (compareEnd == 0) {
				return 0;
			}
			return compareEnd;
		}
		return compareStart;
	}

}
