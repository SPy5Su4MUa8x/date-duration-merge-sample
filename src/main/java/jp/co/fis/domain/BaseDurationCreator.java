package jp.co.fis.domain;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BaseDurationCreator {

	public DateDurations execute() {
		List<DateDuration> durations = new ArrayList<>();
		for (int i = 1; i <= 12; i++) {
			LocalDate from = LocalDate.of(2023, i, 1);
			LocalDate to = from.with(TemporalAdjusters.lastDayOfMonth());
			DateDuration duration = new DateDuration(from, to);
			durations.add(duration);
		}

		log.debug("{}", durations);
		return new DateDurations(durations);
	}

}
