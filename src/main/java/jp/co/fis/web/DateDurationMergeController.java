package jp.co.fis.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import jp.co.fis.domain.BaseDurationCreator;
import jp.co.fis.domain.DateDurationMerge;
import jp.co.fis.domain.DateDurations;
import jp.co.fis.entity.MergeNewDurations;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Controller
public class DateDurationMergeController {

	private final BaseDurationCreator baseDurationCreator;
	private final DateDurationMerge dateDurationMerge;

	@GetMapping(value = "/")
	public ModelAndView initial(ModelAndView mav) {
		DateDurations baseDurations = baseDurationCreator.execute();
		mav.setViewName("before");
		mav.addObject("baseDurations", baseDurations);
		return mav;
	}

	@PostMapping(value = "/merge")
	public ModelAndView merge(ModelAndView mav, @ModelAttribute MergeNewDurations mergeNewDurations) throws Exception {
		// 保持してないのでもう一度呼ぶ。
		DateDurations baseDurations = baseDurationCreator.execute();

		DateDurations mergedDurations = dateDurationMerge.execute(baseDurations,
				mergeNewDurations.removeEmptyNewDuration());

		mav.setViewName("after");
		mav.addObject("newDurations", mergedDurations);
		return mav;
	}

}
