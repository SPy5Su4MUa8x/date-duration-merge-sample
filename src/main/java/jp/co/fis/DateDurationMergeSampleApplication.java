package jp.co.fis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class DateDurationMergeSampleApplication {

	public static void main(String[] args) {
		log.info("■起動！！！");
		SpringApplication.run(DateDurationMergeSampleApplication.class, args);
	}

}
