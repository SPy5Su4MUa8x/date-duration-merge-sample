package jp.co.fis.entity;

import java.util.List;
import java.util.stream.Collectors;

import jp.co.fis.domain.DateDuration;
import jp.co.fis.domain.DateDurations;
import lombok.Data;

@Data
public class MergeNewDurations {

	private List<MergeNewDuration> newDurations;

	public DateDurations removeEmptyNewDuration() {
		List<DateDuration> mergedNewDuration = newDurations.stream().filter(e -> !e.isEmpty())
				.map(e -> new DateDuration(e.getNewFrom(), e.getNewTo()))
				.collect(Collectors.toList());
		return new DateDurations(mergedNewDuration);
	}

}
