package jp.co.fis.entity;

import java.time.LocalDate;

import lombok.Data;

@Data
public class MergeNewDuration {

	private LocalDate newFrom;
	private LocalDate newTo;

	public boolean isEmpty() {
		return newFrom == null && newTo == null;
	}
}
