package jp.co.fis.domain;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DateDurationMergeTest {

	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Autowired
	DateDurationMerge service;

	/**
	 * 【テスト】 <br>
	 * b　　　　　<------------------------------><br>
	 * n <=======><br>
	 * r <=======><------------------------------><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseデータの前日にnewデータが終わる__newデータの後にbaseデータが入る() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2022-01-01", "2022-11-30"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-01-01", "2022-11-30"));
		期待All.add(makeDataModel("2022-12-01", "2023-03-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b　　　　　<------------------------------><br>
	 * n <========><br>
	 * r <========><-----------------------------><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseデータの開始日と同日にnewデータが終わる__newデータの後にbaseデータだがbaseデータの開始日が１日後ろにずれる() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2022-01-01", "2022-12-01"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-01-01", "2022-12-01"));
		期待All.add(makeDataModel("2022-12-02", "2023-03-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b　　　　　<------------------------------><br>
	 * n <==================================><br>
	 * r <==================================><---><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseデータ期間中にnewデータが終わるが終了はnewのほうが早い__newデータの後にbaseデータだがbaseデータの開始日がnewの終了日の１日後ろにずれる() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2022-01-01", "2023-02-28"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-01-01", "2023-02-28"));
		期待All.add(makeDataModel("2023-03-01", "2023-03-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b <------------------------------><br>
	 * n <=======================><br>
	 * r <=======================><-----><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseデータの開始と同日にnewが開始でnewはbaseの期間中に終わる__newデータの後にbaseデータだがbaseデータの開始日がnewの終了日の１日後ろにずれる()
			throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2022-12-01", "2023-02-28"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-12-01", "2023-02-28"));
		期待All.add(makeDataModel("2023-03-01", "2023-03-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b <------------------------------><br>
	 * n <==============================><br>
	 * r <==============================><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseデータの開始と同日にnewが開始でnewとbaseは同日に終わる__newデータのみ残る() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2022-12-01", "2023-03-31"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-12-01", "2023-03-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b <------------------------------><br>
	 * n <=======================================><br>
	 * r <=======================================><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseデータと同日にnewが開始でnewの期間はbaseの期間より長い__newデータのみ残る() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2022-12-01", "2023-04-10"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-12-01", "2023-04-10"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b <------------------------------><br>
	 * n 　　　<===============><br>
	 * r <----><===============><-------><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseデータより後にnewが開始でbaseデータより前にnewデータは終了__basenewbaseで分割される() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2023-01-10", "2023-02-10"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-12-01", "2023-01-09"));
		期待All.add(makeDataModel("2023-01-10", "2023-02-10"));
		期待All.add(makeDataModel("2023-02-11", "2023-03-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b 　　　　　　<------------------------------><br>
	 * n <========><br>
	 * r <========>　<------------------------------><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseデータより前にnewデータが終わるが全く重ならない__newの後にbaseデータが入る() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2022-01-01", "2022-11-20"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-01-01", "2022-11-20"));
		期待All.add(makeDataModel("2022-12-01", "2023-03-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b <------------------------------><br>
	 * n 　　　　　　　　　　　　　　　　　<========><br>
	 * r <------------------------------>　<========><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseデータより後にnewデータが始まるが全く重ならない__baseの後にnewデータが入る() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2023-05-01", "2023-12-31"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-12-01", "2023-03-31"));
		期待All.add(makeDataModel("2023-05-01", "2023-12-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b <----------><br>
	 * b 　　　　　　<------------------------><br>
	 * n 　　　　<========><br>
	 * r 　<----><========><--------------------><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseデータ２期間にまたがるようにnewデータが入る__base１newbase２の順でbase１と２は終了と開始がnewデータによって変わる() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-01-31"));
		baseAll.add(makeDataModel("2023-02-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2023-01-10", "2023-02-20"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-12-01", "2023-01-09"));
		期待All.add(makeDataModel("2023-01-10", "2023-02-20"));
		期待All.add(makeDataModel("2023-02-21", "2023-03-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b１　 <----------><br>
	 * b２　　　　　　　<------------------------><br>
	 * n  　<===========================================><br>
	 * r  　<===========================================><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void newデータの期間がbaseデータの期間を全て含んでいる__newデータだけ残る() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-01-31"));
		baseAll.add(makeDataModel("2023-02-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2022-01-01", "2023-12-31"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-01-01", "2023-12-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b１　<------><br>
	 * b２ 　　　　　　　　<---------------------><br>
	 * n 　　　　<=================><br>
	 * r 　 <---><=================><------------><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseの期間が２つあり間が空いているところにnewデータの期間がはいる__newの前後のbaseデータは期間がカットされbasenewbaseの順になる() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2022-12-31"));
		baseAll.add(makeDataModel("2023-02-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2022-12-20", "2023-02-28"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-12-01", "2022-12-19"));
		期待All.add(makeDataModel("2022-12-20", "2023-02-28"));
		期待All.add(makeDataModel("2023-03-01", "2023-03-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b　　　　　<--------------------------><br>
	 * n１ 　<========><br>
	 * n２ 　　　　　　　　　　　　　　　<========><br>
	 * r 　　<========><----------------><========><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseの前後に重なるようにnewデータ２件あり__baseデータ前後をカットしてnewbasenewの順番になる() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-02-28"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2022-11-01", "2022-12-20"));
		newAll.add(makeDataModel("2023-02-20", "2023-03-31"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-11-01", "2022-12-20"));
		期待All.add(makeDataModel("2022-12-21", "2023-02-19"));
		期待All.add(makeDataModel("2023-02-20", "2023-03-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	/**
	 * 【テスト】 <br>
	 * b　　　　　<--------------------------><br>
	 * n１ 　<========><br>
	 * n２ 　　　　　　<========><br>
	 * r 　　<========><========><-----------><br>
	 * 
	 * @throws Exception
	 */
	@Test
	void baseデータ期間中にnewデータ２件がある__newデータ優先で残期間がbase分で残る() throws Exception {
		// ----- 準備 -----
		// baseデータ
		List<DateDuration> baseAll = new ArrayList<>();
		baseAll.add(makeDataModel("2022-12-01", "2023-03-31"));
		// newデータ
		List<DateDuration> newAll = new ArrayList<>();
		newAll.add(makeDataModel("2023-01-01", "2023-02-28")); // ソートを確認するためnew１と２を順番を逆にした
		newAll.add(makeDataModel("2022-11-01", "2022-12-31"));
		// 期待値
		List<DateDuration> 期待All = new ArrayList<>();
		期待All.add(makeDataModel("2022-11-01", "2022-12-31"));
		期待All.add(makeDataModel("2023-01-01", "2023-02-28"));
		期待All.add(makeDataModel("2023-03-01", "2023-03-31"));

		// ----- 実施 -----
		DateDurations actual = service.execute(new DateDurations(baseAll), new DateDurations(newAll));

		// ----- 検証 -----
		assertEquals(new DateDurations(期待All), actual);
	}

	private DateDuration makeDataModel(String applFmYmd, String applToYmd) throws Exception {
		DateDuration model = new DateDuration(LocalDate.parse(applFmYmd, formatter),
				LocalDate.parse(applToYmd, formatter));
		return model;
	}

}
